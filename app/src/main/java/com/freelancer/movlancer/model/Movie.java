package com.freelancer.movlancer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Movie {

    @Expose
    private Boolean adult;
    @SerializedName("backdrop_path")
    private String backdropPath;
    @SerializedName("genre_ids")
    private List<Long> genreIds;
    @Expose
    private Long id;
    @SerializedName("original_language")
    private String originalLanguage;
    @SerializedName("original_title")
    private String originalTitle;
    @Expose
    private String overview;
    @Expose
    private Double popularity;
    @SerializedName("poster_path")
    private String posterPath;
    @SerializedName("release_date")
    private String releaseDate;
    @Expose
    private String title;
    @Expose
    private Boolean video;
    @SerializedName("vote_average")
    private float voteAverage;
    @SerializedName("vote_count")
    private Long voteCount;

    public Boolean getAdult() {
        return adult;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public List<Long> getGenreIds() {
        return genreIds;
    }

    public Long getId() {
        return id;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public String getOverview() {
        return overview;
    }

    public Double getPopularity() {
        return popularity;
    }

    public String getPosterPath() {
        return posterPath;

    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public String getTitle() {
        return title;
    }

    public Boolean getVideo() {
        return video;
    }

    public float getVoteAverage() {
        return voteAverage;
    }

    public Long getVoteCount() {
        return voteCount;
    }

    public static class Builder {

        private Boolean adult;
        private String backdropPath;
        private List<Long> genreIds;
        private Long id;
        private String originalLanguage;
        private String originalTitle;
        private String overview;
        private Double popularity;
        private String posterPath;
        private String releaseDate;
        private String title;
        private Boolean video;
        private float voteAverage;
        private Long voteCount;

        public Movie.Builder withAdult(Boolean adult) {
            this.adult = adult;
            return this;
        }

        public Movie.Builder withBackdropPath(String backdropPath) {
            this.backdropPath = backdropPath;
            return this;
        }

        public Movie.Builder withGenreIds(List<Long> genreIds) {
            this.genreIds = genreIds;
            return this;
        }

        public Movie.Builder withId(Long id) {
            this.id = id;
            return this;
        }

        public Movie.Builder withOriginalLanguage(String originalLanguage) {
            this.originalLanguage = originalLanguage;
            return this;
        }

        public Movie.Builder withOriginalTitle(String originalTitle) {
            this.originalTitle = originalTitle;
            return this;
        }

        public Movie.Builder withOverview(String overview) {
            this.overview = overview;
            return this;
        }

        public Movie.Builder withPopularity(Double popularity) {
            this.popularity = popularity;
            return this;
        }

        public Movie.Builder withPosterPath(String posterPath) {
            this.posterPath = posterPath;
            return this;
        }

        public Movie.Builder withReleaseDate(String releaseDate) {
            this.releaseDate = releaseDate;
            return this;
        }

        public Movie.Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public Movie.Builder withVideo(Boolean video) {
            this.video = video;
            return this;
        }

        public Movie.Builder withVoteAverage(float voteAverage) {
            this.voteAverage = voteAverage;
            return this;
        }

        public Movie.Builder withVoteCount(Long voteCount) {
            this.voteCount = voteCount;
            return this;
        }

        public Movie build() {
            Movie movie = new Movie();
            movie.adult = adult;
            movie.backdropPath = backdropPath;
            movie.genreIds = genreIds;
            movie.id = id;
            movie.originalLanguage = originalLanguage;
            movie.originalTitle = originalTitle;
            movie.overview = overview;
            movie.popularity = popularity;
            movie.posterPath = posterPath;
            movie.releaseDate = releaseDate;
            movie.title = title;
            movie.video = video;
            movie.voteAverage = voteAverage;
            movie.voteCount = voteCount;
            return movie;
        }

    }
}
