package com.freelancer.movlancer.network;

import com.freelancer.movlancer.model.MovieResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("movie/popular")
    Observable<MovieResponse> getMoviesByPopularity(@Query("page") String page, @Query("api_key") String apiKey);

    @GET("search/movie")
    Observable<MovieResponse> getMoviesByName(@Query("query") String query, @Query("page") String page, @Query("api_key") String apiKey);

}
