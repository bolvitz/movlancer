package com.freelancer.movlancer.utils;

import android.support.annotation.Nullable;
import android.util.Log;

public class CurlPrinter {

    private static final String SINGLE_DIVIDER = "────────────────────────────────────────────";

    private static String sTag = "CURL";

    public static void print(@Nullable String tag, String url, String msg) {
        if (tag != null)
            sTag = tag;

        StringBuilder logMsg = new StringBuilder("\n");
        logMsg.append("\n");
        logMsg.append(SINGLE_DIVIDER);
        logMsg.append("\n");
        logMsg.append(msg);
        logMsg.append(" ");
        logMsg.append(" \n");
        logMsg.append(SINGLE_DIVIDER);
        logMsg.append(" \n ");
        log(logMsg.toString());
    }

    private static void log(String msg) {
        Log.d(sTag, msg);
    }

}
