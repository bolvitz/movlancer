package com.freelancer.movlancer.utils;

import java.util.Locale;

public class StringUtils {

    public static String getMoviePoster(final String posterPath) {
        return String.format(Locale.getDefault(), "https://image.tmdb.org/t/p/w500/%s", posterPath);
    }

}
