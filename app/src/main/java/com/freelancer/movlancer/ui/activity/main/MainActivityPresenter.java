package com.freelancer.movlancer.ui.activity.main;

import com.freelancer.movlancer.BuildConfig;
import com.freelancer.movlancer.model.MovieResponse;
import com.freelancer.movlancer.network.ApiService;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivityPresenter {

    private MainActivityView mainActivityView;

    @Inject
    ApiService mApiService;

    @Inject
    MainActivityPresenter(MainActivityView view) {
        this.mainActivityView = view;
    }

    private <T> void subscribe(Observable<T> observable, Observer<T> observer) {
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    //  get movies according to popularity score
    void getMoviesByPopularity(long page) {
        Observable<MovieResponse> observable = mApiService.getMoviesByPopularity(String.valueOf(page), BuildConfig.API_KEY);
        subscribe(observable, new Observer<MovieResponse>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(MovieResponse movieResponse) {
                mainActivityView.setMovies(movieResponse.getResults());
                mainActivityView.setMoviePage(movieResponse);
            }

            @Override
            public void onError(Throwable e) {
                mainActivityView.onReceivedError(e.getMessage());
            }

            @Override
            public void onComplete() {
            }
        });

    }

    //  get movies according to search field
    void getMoviesByName(String name, long page) {
        Observable<MovieResponse> observable = mApiService.getMoviesByName(name, String.valueOf(page), BuildConfig.API_KEY);
        subscribe(observable, new Observer<MovieResponse>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(MovieResponse movieResponse) {
                mainActivityView.setMovies(movieResponse.getResults());
                mainActivityView.setMoviePage(movieResponse);
            }

            @Override
            public void onError(Throwable e) {
                mainActivityView.onReceivedError(e.getMessage());
            }

            @Override
            public void onComplete() {
            }
        });

    }

}
