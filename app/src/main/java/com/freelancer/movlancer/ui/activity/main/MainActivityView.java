package com.freelancer.movlancer.ui.activity.main;

import com.freelancer.movlancer.model.Movie;
import com.freelancer.movlancer.model.MovieResponse;

import java.util.List;

public interface MainActivityView {

    void setMovies(List<Movie> movies);

    void setMoviePage(MovieResponse movieResponse);

    void onReceivedError(String message);
}
