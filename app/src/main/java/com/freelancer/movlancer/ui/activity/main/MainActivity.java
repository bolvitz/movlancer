package com.freelancer.movlancer.ui.activity.main;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.freelancer.movlancer.MovLancerApplication;
import com.freelancer.movlancer.R;
import com.freelancer.movlancer.di.component.DaggerActivityComponent;
import com.freelancer.movlancer.di.module.MainActivityModule;
import com.freelancer.movlancer.model.Movie;
import com.freelancer.movlancer.model.MovieResponse;
import com.freelancer.movlancer.ui.activity.base.BaseActivity;
import com.freelancer.movlancer.ui.adapter.MovieAdapter;
import com.freelancer.movlancer.utils.EndlessRecyclerViewScrollListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements MainActivityView {

    @Inject
    MainActivityPresenter presenter;

    @BindView(R.id.recyclerViewMovie)
    RecyclerView recyclerViewMovie;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.textViewMovieEmpty)
    TextView textViewMovieEmpty;
    @BindView(R.id.textViewMovieHeader)
    TextView textViewMovieHeader;
    @BindView(R.id.imageViewMovieReload)
    ImageView imageViewMovieReload;

    private MovieAdapter movieAdapter;
    private MovieResponse movieResponse;
    private List<Movie> movieList = new ArrayList<>();
    private String queryMovie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

//      initialize dagger for this activity
        DaggerActivityComponent.builder()
                .appComponent(((MovLancerApplication) getApplication()).getAppComponent())
                .mainActivityModule(new MainActivityModule(this))
                .build().inject(this);

        setSupportActionBar(toolbar);

        initializeUI();

        setupMovieAdapter();

        presenter.getMoviesByPopularity(1);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setQueryHint(getString(R.string.search_movies));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                searchView.clearFocus();
                movieList.clear();
                queryMovie = s;
                presenter.getMoviesByName(queryMovie, 1);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (TextUtils.isEmpty(s)) {
                    queryMovie = s;
                    movieList.clear();
                    presenter.getMoviesByPopularity(1);
                }
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }


    private void initializeUI() {

        imageViewMovieReload.setVisibility(View.GONE);

    }

    private void setupMovieAdapter() {
//      initialize recycler view and its adapter
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        recyclerViewMovie.setLayoutManager(gridLayoutManager);
        movieAdapter = new MovieAdapter(this);
        recyclerViewMovie.setAdapter(movieAdapter);

        recyclerViewMovie.addOnScrollListener(new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (movieResponse != null && movieResponse.getPage() != 0 && movieResponse.getTotalPages() > page) {
                    if (TextUtils.isEmpty(queryMovie)) {
                        presenter.getMoviesByPopularity(movieResponse.getPage() + 1);
                    } else {
                        presenter.getMoviesByName(queryMovie, movieResponse.getPage() + 1);
                    }
                }
            }
        });

    }

    @Override
    public void setMovies(List<Movie> movies) {

        this.movieList.addAll(movies);
        movieAdapter.updateMovieAdapter(this.movieList);

        recyclerViewMovie.setVisibility(movies.isEmpty() ? View.GONE : View.VISIBLE);
        textViewMovieEmpty.setText(movies.isEmpty() ? getString(R.string.no_movies_found) : getString(R.string.loading));
        textViewMovieEmpty.setVisibility(movies.isEmpty() ? View.VISIBLE : View.GONE);
        textViewMovieHeader.setText(TextUtils.isEmpty(queryMovie) ? R.string.most_popular_movies : R.string.found_movies);

    }

    @Override
    public void setMoviePage(MovieResponse movieResponse) {
        this.movieResponse = movieResponse;
    }

    @Override
    public void onReceivedError(String message) {

        imageViewMovieReload.setVisibility(View.VISIBLE);
        recyclerViewMovie.setVisibility(View.GONE);
        textViewMovieEmpty.setVisibility(View.VISIBLE);
        textViewMovieEmpty.setText(R.string.error);
        Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();

    }

    @OnClick(R.id.imageViewMovieReload)
    public void onViewClicked() {

        textViewMovieEmpty.setText(getString(R.string.loading));
        imageViewMovieReload.setVisibility(View.GONE);
        if (TextUtils.isEmpty(queryMovie)) {
            presenter.getMoviesByPopularity(movieResponse != null ? movieResponse.getPage() + 1 : 1);
        } else {
            presenter.getMoviesByName(queryMovie, movieResponse != null ? movieResponse.getPage() + 1 : 1);
        }

    }
}
