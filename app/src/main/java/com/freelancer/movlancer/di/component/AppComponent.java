package com.freelancer.movlancer.di.component;

import com.freelancer.movlancer.di.module.AppModule;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    Retrofit exposeRetrofit();

}
