package com.freelancer.movlancer.di.module;

import com.freelancer.movlancer.di.scope.PerActivity;
import com.freelancer.movlancer.network.ApiService;
import com.freelancer.movlancer.ui.activity.main.MainActivityView;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class MainActivityModule {

    private MainActivityView mainActivityView;

    public MainActivityModule(MainActivityView view) {
        mainActivityView = view;
    }

    @PerActivity
    @Provides
    ApiService provideApiService(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    @PerActivity
    @Provides
    MainActivityView provideView() {
        return mainActivityView;
    }
}
