package com.freelancer.movlancer.di.component;

import com.freelancer.movlancer.di.module.MainActivityModule;
import com.freelancer.movlancer.di.scope.PerActivity;
import com.freelancer.movlancer.ui.activity.main.MainActivity;

import dagger.Component;

@PerActivity
@Component(modules = MainActivityModule.class, dependencies = AppComponent.class)
public interface ActivityComponent {

    void inject(MainActivity activity);

}
