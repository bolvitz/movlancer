package com.freelancer.movlancer;

import android.app.Application;

import com.freelancer.movlancer.di.component.AppComponent;
import com.freelancer.movlancer.di.component.DaggerAppComponent;
import com.freelancer.movlancer.di.module.AppModule;

public class MovLancerApplication extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

//      initialize dagger on application level
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule())
                .build();

    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
